package fds.uml.agl;

public class Courriel {
	// Attributes

	private String adrElect;
	private String titre;
	private String corpsMess;
	private String piecesJointes;

	// Constructor

	public Courriel(String adrElect, String titre, String corpsMess, String piecesJointes)
			throws AdresseMailCourrielInvalide, TitreEstVide, CorpsMessEstVide {
		if (adrElect.matches("[a-zA-Z]+[a-zA-Z0-9]*.[a-zA-Z0-9]+@[a-zA-Z]+.[a-zA-Z]+")) {
			this.adrElect = adrElect;
		} else {
			throw new AdresseMailCourrielInvalide("Cette Adresse est invalide !");
		}
		if (!titre.isEmpty()) {
			this.titre = titre;
		} else {
			throw new TitreEstVide("Ce Titre est vide !");
		}
		if (!corpsMess.isEmpty()) {
			this.corpsMess = corpsMess;
		} else {
			throw new CorpsMessEstVide("Le Corps du Message est vide !");
		}
		this.piecesJointes = piecesJointes;
	}

	// Methods

	public String getAdrElect() {
		return adrElect;
	}

	public String getTitre() {
		return titre;
	}

	public String getCorpsMess() {
		return corpsMess;
	}

	public String getPieceJointe() {
		return piecesJointes;
	}

	public String envoyer() {
					int k = 0;

					String[] ListCorpsMess = this.corpsMess.split(" ");

					while (k < ListCorpsMess.length && ("PJ".equals(ListCorpsMess[k]) || "joint".equals(ListCorpsMess[k])
							|| "jointe".equals(ListCorpsMess[k]))) {
						k += 1;
					}

					String courrier = this.adrElect + "\n" + this.titre + "\n" + this.corpsMess + "\n";

					if (k < ListCorpsMess.length && !piecesJointes.isEmpty()) {
						courrier += this.piecesJointes + "\n";
					}

					return courrier;
				}
}
