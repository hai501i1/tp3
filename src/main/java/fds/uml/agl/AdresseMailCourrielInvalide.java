package fds.uml.agl;

public class AdresseMailCourrielInvalide extends Exception {

	public AdresseMailCourrielInvalide(String string) {
		super(string);
	}

}