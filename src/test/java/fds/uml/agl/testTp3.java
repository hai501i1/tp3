package fds.uml.agl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class testTp3 {
	private static float tolerancePrix = 0.001f;
	private static float toleranceVolume = 0.0000001f;

	Lettre lettre1;
	Lettre lettre2;
	Colis colis1;
	Courriel courriel1;
	ColisExpress colisEx1;
	SacPostal sac1;

	@BeforeEach
	public void setUp() throws AdresseMailCourrielInvalide, TitreEstVide, CorpsMessEstVide {
		lettre1 = new Lettre("Le pere Noel", "famille Kirik, igloo 5, banquise nord", "7877", 25, 0.00018f,
				Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel", "famille Kouk, igloo 2, banquise nord", "5854", 18, 0.00018f,
				Recommandation.deux, true);
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 1024, 0.02f,
				Recommandation.deux, "train electrique", 200);
		courriel1 = new Courriel("guillaume.hakenholz@gmail.com", "Test", "PJ de moi", "PiecesJointes");
		sac1 = new SacPostal();
		sac1.ajoute(colis1);
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
	}

	@Test
	public void testToString() {
		// Arrange

		String c1 = "Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0";
		String l1 = "Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire";
		String l2 = "Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence";

		// Act

		String colis1toString = colis1.toString();
		String lettre1toString = lettre1.toString();
		String lettre2toString = lettre2.toString();

		// Assert

		assertEquals(c1, colis1toString);
		assertEquals(l1, lettre1toString);
		assertEquals(l2, lettre2toString);
	}

	@Test
	public void testAffranchissement() {
		// Arrange

		float c1 = 3.5f;
		float l1 = 1.0f;
		float l2 = 2.3f;

		// Act

		float colis1TarifAff = colis1.tarifAffranchissement();
		float lettre1TarifAff = lettre1.tarifAffranchissement();
		float lettre2TarifAff = lettre2.tarifAffranchissement();

		// Assert

		assertEquals(c1, colis1TarifAff);
		assertEquals(l1, lettre1TarifAff);
		assertEquals(l2, lettre2TarifAff);
	}

	@Test
	public void testRemboursement() {
		// Arrange

		float c1 = 100.0f;
		float l1 = 1.5f;
		float l2 = 15.0f;

		// Act

		float colis1Remb = colis1.tarifRemboursement();
		float lettre1Remb = lettre1.tarifRemboursement();
		float lettre2Remb = lettre2.tarifRemboursement();

		// Assert

		assertEquals(c1, colis1Remb);
		assertEquals(l1, lettre1Remb);
		assertEquals(l2, lettre2Remb);
	}

	@Test
	public void testTarifRamboursement() {
		// Arrange

		// Act

		boolean colis1TarifRemb = Math.abs(colis1.tarifRemboursement() - 100.0f) < tolerancePrix;
		boolean lettre1TarifRemb = Math.abs(lettre1.tarifRemboursement() - 1.5f) < tolerancePrix;
		boolean lettre2TarifRemb = Math.abs(lettre2.tarifRemboursement() - 15.0f) < tolerancePrix;

		// Assert

		assertTrue(colis1TarifRemb);
		assertTrue(lettre1TarifRemb);
		assertTrue(lettre2TarifRemb);
	}

	// Test Classe SacPostal

	@Test
	public void testValeurRamboursement() {
		// Arrange

		// Act

		boolean sac1ValeurRemb = Math.abs(sac1.valeurRemboursement() - 116.5f) < tolerancePrix;

		// Assert

		assertTrue(sac1ValeurRemb);
	}

	@Test
	public void testVolumeSacPostal() {
		// Arrange

		// Act

		boolean sac1Volume = Math.abs(sac1.getVolume() - 0.025359999558422715f) < toleranceVolume;

		// Assert

		assertTrue(sac1Volume);
	}

	@Test
	public void testVolumeSacPostal2() {
		// Arrange

		SacPostal sac2 = sac1.extraireV1("7877");

		// Act

		boolean sac2Volume = sac2.getVolume() - 0.02517999955569394f < toleranceVolume;

		// Assert

		assertTrue(sac2Volume);
	}

	@Test
	public void testAjoutObjetPostal() {
		// Arrange

		// Act

		boolean value = sac1.ajoute(colis1);

		// Assert

		assertTrue(value);
	}

	@Test
	public void testToStringSacPostal() {
		// Arrange
		String s = "Sac \ncapacite: " + 0.5 +
				"\nvolume: " + sac1.getVolume() + "\n" + "[" + colis1.toString() + ", " + lettre1.toString() + ", "
				+ lettre2.toString() + "]" + "\n";

		// Act

		String stringTest = sac1.toString();

		// Assert

		assertEquals(s, stringTest);
	}

	// Test Classe Courriel

	@ParameterizedTest(name = "{0} est une fausse adresse mail !")
	@ValueSource(strings = { "guillaume.hakenholzgmail.com", "guigui.com" })
	public void testMailInvalide(String input) {
		assertThrows(AdresseMailCourrielInvalide.class, () -> {
			new Courriel(input, "titre", "corpsMess", "piecesJointes");
		});
	}

	@Test
	public void testTitreVide() {
		assertThrows(TitreEstVide.class, () -> {
			new Courriel("guillaume.hakenholz@gmail.com", "", "corpsMess", "piecesJointes");
		});
	}

	@Test
	public void testCorpsMessVide() {
		assertThrows(CorpsMessEstVide.class, () -> {
			new Courriel("guillaume.hakenholz@gmail.com", "Titre", "", "piecesJointes");
		});
	}

	@Test
	public void testCreationColisExpressPoidsInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> {
			new ColisExpress("kok", "koko", "647832", 1024, 0.2f, Recommandation.un, "train", 200, true);
		});
	}
}
